<?php
error_reporting(E_ERROR | E_PARSE);
 
// require the setup which has registered the autoloader
require_once dirname(__FILE__) . '/setup.php';


$DeliveryServerendpoint = new MailWizzApi_Endpoint_CustomersDeliveryServers();

$data = [
    'DeliveryServerSmtp' => [
        'name' => 'CAAACAAAA',
        'hostname' => 'sandbox.smtp.mailtrap.io',
        'username' => 'john.doe@mailinator.com',
        'password' => 'superDuperPassword',
        'port' => '25',
        'protocol' => '',
        'from_email' => 'radheshyam.amcodr@gmail.com',
        'from_name' => 'Radheshyam',
        'probability' => '100',
        'hourly_quota' => '0',
        'daily_quota' => '0',
        'monthly_quota' => '0',
        'pause_after_send' => '0',
        'bounce_server_id' => '',
        'tracking_domain_id' => '',
        'use_for' => 'all',
        'signing_enabled' => 'yes',
        'force_from' => 'always',
        'force_sender' => 'no',
        'reply_to_email' => '',
        'force_reply_to' => 'never',
        'max_connection_messages' => '1',
        'customer_id' => 'vs353xqh66d7b',
        'locked' => 'no',
        'warmup_plan_id' => '',
    ],
    'customer' => 'Radheshyam Gohel',
    'warmup-plan' => '',
    'DeliveryServerSmtpType' => 'smtp',
];

$response = $DeliveryServerendpoint->create($data);
$response_data=$response->body->toArray();

//$response = $DeliveryServerendpoint->assingSmtp($response_data['delivery_servers_id'],'vs353xqh66d7b');
//$response_data=$response->body->toArray();

echo json_encode($response_data); 

?>