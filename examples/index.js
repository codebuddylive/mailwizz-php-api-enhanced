const { execSync } = require('child_process');

(async () => {
    const phpScript = './createCustomerWithAPI.php';
    //const args = ['argument1', 'argument2'];    
    const output = JSON.parse(Buffer.from(execSync(`php ${phpScript}`)).toString());//${args.join(' ')}
    console.log('PHP output:', (output.api_key)+" "+(output.customer_uid));
})();
